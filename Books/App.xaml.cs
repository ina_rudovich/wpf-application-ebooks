﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using Books.Model;
using Books.View;
using Books.ViewModel;

namespace Books
{
    public partial class App : Application
    {
        public void OnStartup (object sender, StartupEventArgs e)
        {
            var mainVM = new MainWindowViewModel();
            new MainWindow(mainVM).Show();

        }
    }
}
