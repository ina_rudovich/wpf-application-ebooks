﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;

namespace Books.Model
{
    public class Collection<T> : IList<T>, INotifyPropertyChanged
    {
        private const int DefaultCapacity = 16;

        private T[] items;

        public event PropertyChangedEventHandler PropertyChanged;

        public int Count { get; private set; }

        public bool IsReadOnly
        {
            get { return false; }
        }

        public T this[int index]
        {
            get
            {
                if (index >= Count || index < 0)
                    throw new ArgumentOutOfRangeException();
                return items[index];
            }
            set
            {
                if (index >= Count || index < 0)
                    throw new ArgumentOutOfRangeException();
                items[index] = value;
            }
        }

        public Collection()
        {
            Count = 0;
            items = new T[0];
        }

        public void Add(T item)
        {
            Count++;
            //if (items.Length == Count)
            //{
            //    Array.Resize(ref items, Count << 1);
            //}
            Array.Resize(ref items, Count);

            items[Count-1] = item;
        }
        
        public int IndexOf(T item)
        {
            for (int index = 0; index < Count; ++index)
            {
                if (items[index].Equals(item))
                {
                    return index;
                }
            }
            return -1;

            //int counter = 0;
            //foreach (T i in this)
            //    if (i.Equals(item))
            //        return counter;
            //    else counter++;
            //return -1;
        }

        public bool Remove(T item)
        {

            int index = IndexOf(item);
            if (index >= 0)
            {
                RemoveAt(index);
                return true;
            }
            return false;
        }

        public void RemoveAt(int index)
        {
            if (index < 0 || index > (Count-1))
                throw new IndexOutOfRangeException();
            else
            {
                int size = items.Length - 1;
                Array.Copy(items, index + 1, items, index, size - index);
                Count--;
                items[size] = default(T);
            }
        
        }

        public IEnumerator<T> GetEnumerator()
        {
            for (int i = 0; i < items.Length; i++)
                 yield return items[i];
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();

        }

        public void Insert(int index, T item)
        {
            if (index < 0 || index > (Count - 1) )
                throw new IndexOutOfRangeException();
            else
            {
                // todo: check this method
                if (items.Length == Count)
                {
                    Array.Resize(ref items, Count << 1);
                }
                Count++;

                Array.Copy(items, index , items, index + 1, Count - index);

                items[index] = item;
            }

        }

        public void Clear()
        {
            items = new T[0];
        }

        public bool Contains(T item)
        {
            return IndexOf(item) != -1;
        }

        public void CopyTo(T[] array, int index)
        {
            if (index < 0 || index > (Count - 1) )
                throw new IndexOutOfRangeException();
            else
            {
                if (index > array.Length - Count)
                    throw new ArgumentException();
                else
                {
                    Array.Copy(items, 0, array, index, Count);
                }
            }
        }
    }

    public static class BookCollectionExtensions
    {
        public static Book FindByName(this Collection<Book> bookCollection, string name)
        {
            return bookCollection.FirstOrDefault(x => x.Name == name);
        }
    }
}