﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
using System.Runtime.CompilerServices;


namespace Books.Model
{
    public class Author: IEquatable<Author>,INotifyPropertyChanged
    {

        private string _name;
        private DateTime _birthday;
        //private BookCollection _books;
        private string _image;
        private Collection<string> _books;
        public string Name
        {
            get { return _name; }
            set
            {
                _name = value;
                OnPropertyChanged();
            }
        }

        public DateTime Birthday
        {
            get { return _birthday; }
            set
            {
                _birthday = value;
                OnPropertyChanged();
            }
        }

        //public BookCollection Books
        //{
        //    get { return _books; }
        //    set
        //    {
        //        _books = value;
        //        OnPropertyChanged();
        //    }
        //}
        public Collection<string> Books
        {
            get { return _books; }
            set
            {
                _books = value;
                OnPropertyChanged();
            }
        }


        public string Image
        {
            get { return _image; }
            set
            {
                _image = value;
                OnPropertyChanged();
                
            }
        }
        [field:NonSerialized]
        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged([CallerMemberName]string prop = "")
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(prop));
        }



        public Author ()
        {
            Name = "No Info";
            // Books = new BookCollection();
            Books = new Collection<string>();
            Image = @"D:\lab\isp_labs\NoImage.jpg";
        }

        public Author (string name)
        {
            
            Name = name;
            //Books = new BookCollection();
            Books = new Collection<string>();
            Image = @"D:\lab\isp_labs\NoImage.jpg";
        }

        public Author (string name, DateTime birthday)
        {
            Name = name;
            Birthday = birthday;
            //Books = new BookCollection();
            Books = new Collection<string>();
            Image = @"D:\lab\isp_labs\NoImage.jpg";
        }

        public Author(string name, DateTime birthday,string image)
        {
            Name = name;
            Birthday = birthday;
           // Books = new BookCollection();
           Books = new Collection<string>();
            Image =image;
        }



        public bool Equals(Author other)
        {
            if (other == null)
                return false;
            if (other.Name == Name)
                return true;
            else
                return false;
        }

        public override bool Equals(object obj)
        {
            Author book = obj as Author;
            if (book == null)
                return false;
            else return Equals(book);
        }

        public override int GetHashCode()
        {
            return Name.GetHashCode();
        }

    }
}
