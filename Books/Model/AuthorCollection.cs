﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace Books.Model
{
    public class AuthorCollection:Collection<Author>
    {
        public override void Add(Author author)
        {

            if (!Contains(author))
                base.Add(author);
        }

    }
}
