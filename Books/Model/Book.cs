﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace Books.Model
{
    public class Book : IEquatable<Book>, INotifyPropertyChanged
    {
        private string _name;
        private string _isbn;
        private AuthorCollection _authors;
        private string _publisher;
        private int _pagecount;
        private string _teg;
        private DateTime _yearofpublishing;
        private string _image;


        public string Name
        {
            get { return _name; }
            set
            {
                _name = value;
                OnPropertyChanged();
            }
        }

        public string ISBN
        {
            get { return _isbn; }
            set
            {
                _isbn = value;
                OnPropertyChanged();
            }
        }

        public AuthorCollection Authors
        {
            get { return _authors; }
            set
            {
                _authors = value;
                OnPropertyChanged();
            }
        }

        public string Publisher
        {
            get { return _publisher; }
            set
            {
                _publisher = value;
                OnPropertyChanged();
            }
        }       

        public int PageCount
        {
            get { return _pagecount; }
            set
            {
                _pagecount = value;
                OnPropertyChanged();
            }
        }

        public string Teg
        {
            get { return _teg; }
            set
            {
                _teg = value;
                OnPropertyChanged();
            }
        }

        public DateTime YearOfPublication
        {
            get { return _yearofpublishing; }
            set
            {
                _yearofpublishing = value;
                OnPropertyChanged();
            }
        }

        public string Image
        {
            get { return _image; }
            set
            {
                _image = value;
                OnPropertyChanged();
            }
        }

        [field: NonSerialized]
        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged([CallerMemberName]string prop = "")
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(prop));
        }


        public Book()
        {
            Name = "No Info";
            ISBN = "No Info";
            PageCount = 0;
            Teg = "no Info";
            Publisher = "No Info";
            Authors = new AuthorCollection();
            Image = @"D:\lab\isp_labs\NoImage.jpg";
        }

        public Book(string name, string isbn, int page, string teg,string publisher, DateTime yearofpublication, string authors)
        {
            Name = name;
            ISBN = isbn;
            PageCount = page;
            Teg = teg;
            YearOfPublication = yearofpublication;
            Authors = new AuthorCollection();
            Publisher = publisher;
            string[] a = authors.Split(',');
            foreach (string i in a)
            {
                Author A = new Author(i);
                Authors.Add(A);
            }
            Image = @"D:\lab\isp_labs\NoImage.jpg";
        }

        public Book(string name)
        {
            Name = name;
            Image = @"D:\lab\isp_labs\NoImage.jpg";
        }

        public bool Equals(Book other)
        {
            if (other == null)
                return false;
            if (other.Name == Name)
                return true;
            else
                return false;
        }

        public override bool Equals(object obj)
        {
            Book book = obj as Book;
            if (book == null)
                return false;
            else return Equals(book);
        }

        public override int GetHashCode()
        {
            return Name.GetHashCode();
        }
    }
}

