﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Books.Model
{
    [Serializable]
    public class BookCollection:Collection<Book>
    {
        public override void Add(Book book)
        {
            if(!Contains(book))
                base.Add(book);
            
               
        }
        
    }
}
