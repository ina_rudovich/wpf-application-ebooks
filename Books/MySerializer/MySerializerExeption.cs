﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Books.MySerializer
{
    public class MySerializerException : Exception
    {
        public MySerializerException() : base() { }
        public MySerializerException(string ms) : base(ms) { }
    }
}
