﻿using Books.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Books.View
{
    /// <summary>
    /// Логика взаимодействия для AddAuthor.xaml
    /// </summary>
    public partial class AddAuthor : Window
    {
        public AddAuthorViewModel AddAuthorVM;
        

        public AddAuthor(AddAuthorViewModel addAuthorVM)
        {
            AddAuthorVM = addAuthorVM;
            AddAuthorVM.AddAuthorWND = this;
            DataContext = AddAuthorVM;

            InitializeComponent();
        }
    }
}
