﻿using Books.Model;
using Books.ViewModel;
using Books.View;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.IO;

namespace Books.View
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindowViewModel ViewModel { get; }
        public BookCollection collection = new BookCollection();       

        public MainWindow(MainWindowViewModel viewModel)
        {
            ViewModel = viewModel;
            LanguageSelection ls = new LanguageSelection();
            ls.ShowDialog();
            //DataContext = new MainWindowViewModel();
            
            InitializeComponent();
        }

       /* private void Click_RU(object sender, RoutedEventArgs e)
        {
            //Properties.Settings.Default.DefaultL = System.Globalization.CultureInfo.GetCultureInfo("ru-RU");
            //Properties.Settings.Default.Save();
            System.Threading.Thread.CurrentThread.CurrentUICulture = System.Globalization.CultureInfo.GetCultureInfo("en-US");
            MyWND.UpdateLayout();
        }
        private void Click_EN(object sender, RoutedEventArgs e)
        {
            //System.Threading.Thread.CurrentThread.CurrentUICulture = System.Globalization.CultureInfo.GetCultureInfo("en-US");
            //InitializeComponent();
            Properties.Settings.Default.DefaultL = System.Globalization.CultureInfo.GetCultureInfo("en-US");
            Properties.Settings.Default.Save();

        }*/

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            using (FileStream stream = new FileStream("S_Books.dat", FileMode.OpenOrCreate))
            {
                try
                {
                    object obj = MySerializer.MySerializer.Deserialize(stream);
                    ViewModel.Library = (Library)obj;

                    foreach (var book in ViewModel.Library.books)
                    {
                        ViewModel.BookViewModel.BooksCollection.Add(book);
                    }

                    foreach (var author in ViewModel.Library.authors)
                    {
                        ViewModel.AuthorViewModel.AuthorCollection.Add(author);
                    }

                }
                catch (EndOfStreamException)
                {
                    Console.WriteLine("End");
                }

            }
        }

        private void Window_Closed(object sender, EventArgs e)
        {
            ViewModel.Library = new Library();
            foreach(var book in ViewModel.BookViewModel.BooksCollection)
            {
                ViewModel.Library.books.Add(book);
            }
            foreach(var author in ViewModel.AuthorViewModel.AuthorCollection)
            {
                ViewModel.Library.authors.Add(author);
            }

            using (FileStream stream = new FileStream("S_Books.dat", FileMode.OpenOrCreate))
            {
                MySerializer.MySerializer.Serialize(stream, ViewModel.Library);
            }
            ViewModel.Close();
            ViewModel.CloseBin();

        }

    }
}
