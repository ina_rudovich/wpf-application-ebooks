﻿using Books.Model;
using Books.View;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using System.Windows.Forms;

namespace Books.ViewModel
{
    public class AddAuthorViewModel:ViewModelBase
    {
        public MainWindowViewModel MainWindowVM { get; set; }

        public AddAuthor AddAuthorWND { get; set; }

        private Author _Author;

        public Author Author
        {
            get { return _Author; }
            set { _Author = value; OnPropertyChanged(); }
        }

        #region Commands
        public ICommand AddAuthorToCollection_Command { get; set; }

        public void AddAuthorToCollection_Method()
        {
            MainWindowVM.AuthorViewModel.AuthorCollection.Add(Author);
            AddAuthorWND.Close();
        }

        public ICommand ChangeImage_Command { get; set; }

        public void ChangeImage_Method()
        {
            OpenFileDialog dialog = new OpenFileDialog();
            dialog.InitialDirectory = "d:\\";
            dialog.Filter = "Image files (*.jpg)|*.jpg|All Files (*.*)|*.*";
            dialog.RestoreDirectory = true;

            if (dialog.ShowDialog() == DialogResult.OK)
            {
                Author.Image = dialog.FileName;
            }
        }
        #endregion Commands

        public AddAuthorViewModel(MainWindowViewModel MainW_VM)
        {
            MainWindowVM = MainW_VM;
            Author = new Author();
            AddAuthorToCollection_Command = new Command(arg => AddAuthorToCollection_Method());
            ChangeImage_Command = new Command(arg => ChangeImage_Method());
            
        }
    
    }
}
