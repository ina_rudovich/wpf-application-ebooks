﻿using Books.Model;
using Books.View;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Input;

namespace Books.ViewModel
{
    public class AddBookViewModel:ViewModelBase
    {
        public AddBook AddBookWND { get; set;}
        public MainWindowViewModel MainWindowVM { get; set;}

        private Book _Book;

        public Book Book
        {
            get { return _Book; }
            set { _Book = value; OnPropertyChanged(); }
        }

        #region Commands
        public ICommand AddExistingAuthor_Command { get; set; }

        public void AddExistingAuthor_Method()
        {
            Book.Authors.Add(MainWindowVM.AuthorViewModel.SelectedAuthor);
            MainWindowVM.AuthorViewModel.SelectedAuthor.Books.Add(Book.Name);
        }

        public ICommand AddBookToCollection_Command { get; set; }

        public void AddBookToCollection_Method()
        {
            MainWindowVM.BookViewModel.BooksCollection.Add(Book);
            AddBookWND.DialogResult = true;
        }


        public ICommand ShowAddAuthorWindow_Command { get; set; }


        public void ShowAddAuthorWindow_Method()
        {
            AddAuthorViewModel addAuthorViewModel = new AddAuthorViewModel(MainWindowVM);
            AddAuthor addAuthor = new AddAuthor(addAuthorViewModel);

            addAuthor.ShowDialog();
        }

        public ICommand ChangeImage_Command { get; set; }

        public void ChangeImage_Method()
        {
            OpenFileDialog dialog = new OpenFileDialog();
            dialog.InitialDirectory = "d:\\";
            dialog.Filter = "Image files (*.jpg)|*.jpg|All Files (*.*)|*.*";
            dialog.RestoreDirectory = true;

            if (dialog.ShowDialog() == DialogResult.OK)
            {
                Book.Image = dialog.FileName;
            }
        }
        #endregion Commands

        public AddBookViewModel(MainWindowViewModel mainWindowVM)
        {
            Book = new Book();
            MainWindowVM = mainWindowVM;
            AddExistingAuthor_Command = new Command(arg => AddExistingAuthor_Method());
            AddBookToCollection_Command = new Command(args => AddBookToCollection_Method());
            ShowAddAuthorWindow_Command = new Command(args => ShowAddAuthorWindow_Method());
            ChangeImage_Command = new Command(arg => ChangeImage_Method());
        }
    }
}
