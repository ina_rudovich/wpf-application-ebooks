﻿using Books.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Input;

namespace Books.ViewModel
{
    public class AuthorViewModel:ViewModelBase
    {
        private AuthorCollection _AuthorCollection = new AuthorCollection();

        public AuthorCollection AuthorCollection
        {
            get { return _AuthorCollection; }
            set { _AuthorCollection = value; OnPropertyChanged(); }
        }

        private Author _Author;

        public Author SelectedAuthor
        {
            get { return _Author; }
            set { _Author = value; OnPropertyChanged(); }
        }

        #region Command
        public ICommand ChangeImage_Command { get; set; }
        #endregion Command

        public void ChangeImage_Method()
        {
            OpenFileDialog dialog = new OpenFileDialog();
            dialog.InitialDirectory = "d:\\";
            dialog.Filter = "Image files (*.jpg)|*.jpg|All Files (*.*)|*.*";
            dialog.RestoreDirectory = true;

            if (dialog.ShowDialog() == DialogResult.OK)
            {
                SelectedAuthor.Image = dialog.FileName;
            }
        }

        public AuthorViewModel()
        {
            //Author a = new Author("author 1", new DateTime(2000, 1, 1));
            //Author b = new Author("author 2", new DateTime(2010, 1, 1));
            //AuthorCollection.Add(a);
            //AuthorCollection.Add(b);
            ChangeImage_Command = new Command(arg => ChangeImage_Method());

        }
    }
}
