﻿using Books.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Input;

namespace Books.ViewModel
{
    public class BookViewModel:ViewModelBase
    {   
        private BookCollection _BooksCollection = new BookCollection();

        public BookCollection BooksCollection
        {
            get { return _BooksCollection; }
            set { _BooksCollection= value; OnPropertyChanged(); }
        }

        private Book _Book;

        public Book SelectedBook
        {
            get{ return _Book; }
            set{ _Book = value; OnPropertyChanged(); }
        }

        #region Command
        public ICommand ChangeImage_Command { get; set; }

        public void ChangeImage_Method()
        {
            OpenFileDialog dialog = new OpenFileDialog();
            dialog.InitialDirectory = "d:\\";
            dialog.Filter = "Image files (*.jpg)|*.jpg|All Files (*.*)|*.*";
            dialog.RestoreDirectory = true;

            if (dialog.ShowDialog() == DialogResult.OK)
            {
                SelectedBook.Image = dialog.FileName;
            }
        }
        #endregion Command

        public BookViewModel()
        {
            

            //Book b = new Book("ABC","1", 100, "aaaaaa","Publisher1" ,new DateTime(2000, 1, 1),"A. AAA, B. BBBB");
            //Book c = new Book("10", "20", 30, "40", "Publisher2", new DateTime(2000, 1, 1), "d, e, f");
           
            //BooksCollection.Add(b);
            //BooksCollection.Add(c);

            ChangeImage_Command = new Command(arg => ChangeImage_Method());
        }

    }
}
