﻿using Books.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Books.ViewModel
{
    public class Library
    {
        public BookCollection books { get; set; }
        public AuthorCollection authors { get; set; }
        public Library()
        {
            books = new BookCollection();
            authors = new AuthorCollection();
        }
    }
}
