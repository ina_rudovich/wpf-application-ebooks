﻿using Books.Model;
using Books.View;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using System.Windows.Forms;


namespace Books.ViewModel
{
    public class MainWindowViewModel:ViewModelBase
    {
        private BookViewModel _BookViewModel;
        private AuthorViewModel _AuthorViewModel;

        public Library Library;

        public BookViewModel BookViewModel
        {
            get { return _BookViewModel; }
            set { _BookViewModel = value; OnPropertyChanged(); }
        }

        public AuthorViewModel AuthorViewModel
        {
            get { return _AuthorViewModel; }
            set { _AuthorViewModel = value; OnPropertyChanged(); }
        }


        #region Commands
        public ICommand ShowAddAuthorWindow_Command { get; set; }
        public ICommand RemoveBookFromCollection_Command { get; set; }
        public ICommand RemoveAuthorFromCollection_Command { get; set; }
        public ICommand ShowAddBookWindow_Command { get; set; }

        public ICommand DownLoad_Command { get; set; }
        public ICommand Save_Command { get; set; }

        #region LINQ
        public ICommand SortBookCollection_Command { get; set; }
        public ICommand SortBookByData_Command { get; set; }
        public ICommand SortAuthorsByName_Command { get; set; }
        public ICommand SortAuthorsByBirthday_Command { get; set; }
        public ICommand RemoveBooksWithoutAuthors_Command { get; set; }
        public ICommand RemoveAuthorsWithoutBooks_Command { get; set; }
        public ICommand BiggestBook_Command { get; set; }
        #endregion LINQ

        public void ShowAddAuthorWindow_Method()
        {
            AddAuthorViewModel addAuthorViewModel = new AddAuthorViewModel(this);
            AddAuthor addAuthor = new AddAuthor(addAuthorViewModel);

            addAuthor.ShowDialog();
        }       

        public void RemoveAuthorFromCollection_Method ()
        {
            if (AuthorViewModel.SelectedAuthor == null) return;
            //foreach(var Book in AuthorViewModel.SelectedAuthor.Books)
            //{
            //    Book.Authors.Remove(AuthorViewModel.SelectedAuthor);
            //}
            foreach(var book in BookViewModel.BooksCollection)
            {
                book.Authors.Remove(AuthorViewModel.SelectedAuthor);
            }

            AuthorViewModel.AuthorCollection.Remove(AuthorViewModel.SelectedAuthor);
        }

        public void ShowAddBookWindow_Method()
        {
            AddBookViewModel addBookViewModel = new AddBookViewModel(this);
            AddBook addBook = new AddBook(addBookViewModel);
            addBook.ShowDialog();
        }

        public void RemoveBookFromCollection_Method()
        {
            if (BookViewModel.SelectedBook == null) return;
            foreach(var author in BookViewModel.SelectedBook.Authors)
            {
                author.Books.Remove(BookViewModel.SelectedBook.Name);
            }

            BookViewModel.BooksCollection.Remove(BookViewModel.SelectedBook);
        }

        public void Download_Method()
        {
            OpenFileDialog dialog = new OpenFileDialog();
            dialog.InitialDirectory = "d:\\";
            dialog.Filter = "File (*.txt)|*.txt|BinFile(*.bin)|*.bin";
            dialog.RestoreDirectory = true;

            if (dialog.ShowDialog() == DialogResult.OK)
            {
                string path = dialog.FileName;
                if (path.Contains(".txt"))
                {
                    Loaded(path);
                }
                else
                {
                    if (path.Contains(".bin"))
                    {
                        LoadedBin(path);
                    }
                }

            }
        }

        public void Save_Method()
        {
            SaveFileDialog dialog = new SaveFileDialog();
            dialog.InitialDirectory = "d:\\";
            dialog.Filter = "File (*.txt)|*.txt|BinFile(*.bin)|*.bin";
            dialog.RestoreDirectory = true;

            if (dialog.ShowDialog() == DialogResult.OK)
            {
                string path = dialog.FileName;
                if (path.Contains(".txt"))
                {
                    Close(path);
                }
                else
                {
                    if (path.Contains(".bin"))
                    {
                        CloseBin(path);
                    }
                }

            }
        }

        #region LINQ

        public void SortBookCollection_Method()
        {
            var NewBookCollection = from book in BookViewModel.BooksCollection
                                    orderby book.Name
                                    select book;
            BookViewModel.BooksCollection = new BookCollection();
            foreach(Book book in NewBookCollection)
            {
                BookViewModel.BooksCollection.Add(book);
            }
        }

        public void SortBookByData_Method()
        {
            var NewBookCollection = from book in BookViewModel.BooksCollection
                                    orderby book.YearOfPublication descending
                                    select book;
            BookViewModel.BooksCollection = new BookCollection();
            foreach (Book book in NewBookCollection)
            {
                BookViewModel.BooksCollection.Add(book);
            }
        }

        public void SortAuthorsByName_Method()
        {
            var NewAuthorCollection = from author in AuthorViewModel.AuthorCollection
                                      orderby author.Name 
                                      select author;
            AuthorViewModel.AuthorCollection = new AuthorCollection();
            foreach (Author author in NewAuthorCollection)
            {
                AuthorViewModel.AuthorCollection.Add(author);
            }
        }

        public void SortAuthorsByBirthday_Method()
        {
            var NewAuthorCollection = from author in AuthorViewModel.AuthorCollection
                                      orderby author.Birthday descending
                                      select author;
            AuthorViewModel.AuthorCollection = new AuthorCollection();
            foreach (Author author in NewAuthorCollection)
            {
                AuthorViewModel.AuthorCollection.Add(author);
            }
        }

        public void RemoveBooksWithoutAuthors_Method()
        {
            var NewBookCollection = BookViewModel.BooksCollection.Where(book => (book.Authors.Count == 0));
            while(NewBookCollection.Count() != 0)
                foreach(Book book in NewBookCollection)
                {
                    BookViewModel.BooksCollection.Remove(book);
                }
        }

        public void RemoveAuthorsWithoutBooks_Method()
        {
            var NewAuthorCollection = AuthorViewModel.AuthorCollection.Where(author => (author.Books.Count == 0));
            while(NewAuthorCollection.Count() != 0) 
                foreach(Author author in NewAuthorCollection)
                {
                    AuthorViewModel.AuthorCollection.Remove(author);
                }
        }

        public void BiggestBook_Method()
        {
            int pages = BookViewModel.BooksCollection.Max(x => x.PageCount);
            var _book = BookViewModel.BooksCollection.SkipWhile(x => (x.PageCount != pages)).Take(1);
            foreach(Book book in _book)
            {
                BookViewModel.SelectedBook = book;
            }
            
        }


        #endregion LINQ
        #endregion Command

        public MainWindowViewModel()
        {
            

            BookViewModel = new BookViewModel();
            AuthorViewModel = new AuthorViewModel();
            Library = new Library();
            BookViewModel.BooksCollection = Library.books;
            AuthorViewModel.AuthorCollection = Library.authors;
            ShowAddAuthorWindow_Command = new Command(arg => ShowAddAuthorWindow_Method());
            RemoveAuthorFromCollection_Command = new Command(arg => RemoveAuthorFromCollection_Method());

            RemoveBookFromCollection_Command = new Command(arg => RemoveBookFromCollection_Method());
            ShowAddBookWindow_Command = new Command(arg => ShowAddBookWindow_Method());

            SortBookCollection_Command = new Command(arg => SortBookCollection_Method());
            SortBookByData_Command = new Command(arg => SortBookByData_Method());
            RemoveBooksWithoutAuthors_Command = new Command(arg => RemoveBooksWithoutAuthors_Method());
            BiggestBook_Command = new Command(arg => BiggestBook_Method());
            

            SortAuthorsByName_Command = new Command(arg => SortAuthorsByName_Method());
            SortAuthorsByBirthday_Command = new Command(arg => SortAuthorsByBirthday_Method());
            RemoveAuthorsWithoutBooks_Command = new Command(arg => RemoveAuthorsWithoutBooks_Method());

            DownLoad_Command = new Command(arg => Download_Method());
            Save_Command = new Command(arg => Save_Method());
           

        }

        #region SaveInfo
        public void Close(string Path = "Books.txt")
        {
            
            using (StreamWriter Writer = new StreamWriter(Path, false, Encoding.Default))
            {
                foreach(var book in BookViewModel.BooksCollection)
                {
                    Writer.WriteLine(book.Name);
                    Writer.WriteLine(book.ISBN);
                    Writer.WriteLine(book.Publisher);
                    Writer.WriteLine(book.PageCount);
                    Writer.WriteLine(book.Teg);
                    Writer.WriteLine(book.YearOfPublication);
                    Writer.WriteLine(book.Image);
                    foreach(var author in book.Authors)
                    {
                        Writer.WriteLine(author.Name);
                        Writer.WriteLine(author.Birthday);
                        Writer.WriteLine(author.Image);
                    }
                    Writer.WriteLine("");
                }
            }
        }

        public void CloseBin(string Path = "Books.bin")
        {
            
            using (var fileStream = File.Open(Path, FileMode.OpenOrCreate))
            using (BinaryWriter Writer = new BinaryWriter(fileStream))
            {
                foreach (var book in BookViewModel.BooksCollection)
                {
                    Writer.Write(book.Name);
                    Writer.Write(book.ISBN);
                    Writer.Write(book.Publisher);
                    Writer.Write(book.PageCount);
                    Writer.Write(book.Teg);
                    Writer.Write(book.YearOfPublication.ToString());
                    Writer.Write(book.Image);
                    foreach (var author in book.Authors)
                    {
                        Writer.Write(author.Name);
                        Writer.Write(author.Birthday.ToString());
                        Writer.Write(author.Image);
                    }
                    Writer.Write("");
                }
            }
        }
        #endregion SaveInfo

        #region AddInfo
        public void Loaded(string Path = "Books.txt")
        {
            
            using (StreamReader Reader = new StreamReader(Path, System.Text.Encoding.Default))
            {
                string line;
                while((line = Reader.ReadLine()) != null)
                {
                    Book book = new Book();
                    book.Name = line;
                    book.ISBN = Reader.ReadLine();
                    book.Publisher = Reader.ReadLine();
                    book.PageCount = Int32.Parse(Reader.ReadLine());
                    book.Teg = Reader.ReadLine();
                    book.YearOfPublication = DateTime.Parse(Reader.ReadLine());
                    book.Image = Reader.ReadLine();
                    while ((line = Reader.ReadLine()) != "")
                    {
                        Author author = new Author();
                        author.Name = line;
                        author.Birthday = DateTime.Parse(Reader.ReadLine());
                        author.Image = Reader.ReadLine();
                        book.Authors.Add(author);
                        AuthorViewModel.AuthorCollection.Add(author);
                        foreach(var a in AuthorViewModel.AuthorCollection)
                        {
                            if (a.Equals(author))
                            {
                                a.Books.Add(book.Name);
                            }
                        }
                    }
                    BookViewModel.BooksCollection.Add(book);

                }
            }
        }

        public void LoadedBin(string Path = "Books.bin")
        {
            
            using (BinaryReader Reader = new BinaryReader(File.Open(Path, FileMode.Open)))
            {
                string line;
                while ((line = Reader.ReadString()) != null)
                {
                    Book book = new Book();
                    book.Name = line;
                    book.ISBN = Reader.ReadString();
                    book.Publisher = Reader.ReadString();
                    book.PageCount = Reader.ReadInt32();
                    book.Teg = Reader.ReadString();
                    book.YearOfPublication = DateTime.Parse(Reader.ReadString());
                    book.Image = Reader.ReadString();
                    while ((line = Reader.ReadString()) != "")
                    { 
                        Author author = new Author();
                        author.Name = line;
                        author.Birthday = DateTime.Parse(Reader.ReadString());
                        author.Image = Reader.ReadString();
                        book.Authors.Add(author);
                        AuthorViewModel.AuthorCollection.Add(author);
                        foreach (var a in AuthorViewModel.AuthorCollection)
                        {
                            if (a.Equals(author))
                            {
                                a.Books.Add(book.Name);
                            }
                        }
                    }
                    BookViewModel.BooksCollection.Add(book);
                }
            }
        }
        #endregion AddInfo
        

    }
}
